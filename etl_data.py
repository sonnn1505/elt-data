from data_mysql import *

from prefect import task, Flow

## extract
@task
def get_complaint_data():
    # customer
    sql = "select cu.id as customer_id, cu.name as customer_name,ad.province,ad.city,ad.distrist,ad.ward,cu.address_detail  from customer cu inner join address ad on ad.id = cu.address_id"
    data_source = get_data_source(sql)
    print(data_source)
    return data_source

## transform
@task
def parse_complaint_data(raw):
    pass

## load
@task
def store_complaints(raw):
    sql = "INSERT INTO stg_customer (customr_id, name, province, city, distrist, ward, address) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    insert_dest_source(sql,raw)

with Flow("etl flow") as f:
    raw = get_complaint_data()
    #parsed = parse_complaint_data(raw)
    store_complaints(raw)

f.run()
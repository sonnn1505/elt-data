CREATE SCHEMA `etl_source` ;
 
-- address

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `province` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `distrist` varchar(45) DEFAULT NULL,
  `ward` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `address` VALUES (1,'TP HỒ CHÍ MINH','TP HCM','Quận 1','Phường 1'),(2,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 1','Phường 1'),(3,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 1','Phường 2'),(4,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 1','Phường 3'),(5,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 1','Phường 4'),(6,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 1','Phường 5'),(7,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 1','Phường 6'),(8,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 2','Phường 1'),(9,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 3','Phường 2'),(10,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 4','Phường 3'),(11,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 5','Phường 4'),(12,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 6','Phường 5'),(13,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 7','Phường 6'),(14,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 8','Phường 7'),(15,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 9','Phường 8'),(16,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 10','Phường 9'),(17,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 11','Phường 10'),(18,'HỒ CHÍ MINH','TP Hồ Chí Minh','Quận 12','Phường 11'),(19,'Bình Dương','TP Thuận An','Thuận An','Vĩnh Phú'),(20,'Bình Dương','TP Thuận An','Thuận An','Vĩnh Phú'),(21,'Bình Dương','TP Thuận An','Thuận An','Vĩnh Phú'),(22,'Bình Dương','TP Thuận An','Thuận An','Vĩnh Phú'),(23,'Bình Dương','TP Thuận An','Thuận An','Vĩnh Phú');


---customer

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address_id` int NOT NULL,
  `address_detail` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `adress_idx` (`address_id`),
  CONSTRAINT `adres_pk` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `customer` WRITE;
INSERT INTO `customer` VALUES (1,'Nguyễn Văn A',1,'132 Hàm nghi'),(2,'Nguyễn Văn B',2,'226 Nam kỳ khởi nghĩa');
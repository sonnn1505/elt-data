import mysql.connector

# get data from source
def get_data_source(sql):
    # Connect DB source
    dbsource = mysql.connector.connect(host="localhost", user="sonroot", passwd="root",database="etl_source")
    cursor_source = dbsource.cursor()
    # Execute SQL select statement
    cursor_source.execute(sql)
    results = cursor_source.fetchall()
    # Close the connection
    dbsource.close()
    return results

# insert data to dest
def insert_dest_source(sql,object_list):
    # Connect DB dest
    dbdest = mysql.connector.connect(host="localhost", user="sonroot", passwd="root",database="etl_dest")
    cursor_dest = dbdest.cursor()

    cursor_dest.executemany(sql,object_list)
    dbdest.commit()
    dbdest.close()
    return True
